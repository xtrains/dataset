# import the necessary packages
from scipy.stats import truncnorm
from skimage.io import imread
import numpy as np
from os import listdir
import math


def get_truncated_normal(mean=0, sd=1, low=0, upp=10):
    return truncnorm((low - mean)/sd, (upp - mean)/sd, loc=mean, scale=sd)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def compress_images(path_to_images, output_path, images_per_npz=5_000):
    print("[INFO] Loading images...")
    image_names = listdir(path_to_images)

    for i, chunk in enumerate(chunks(image_names, images_per_npz)):
        images = []
        for img_path in chunk:
            # load the image and store it in the data list
            image = imread(path_to_images + img_path)
            images.append(image)
        print("[INFO] Processed {}/{}...".format(i + 1, math.ceil(len(image_names) / images_per_npz)))
        images = np.asarray(images)
        # save data list as npz
        np.savez_compressed(output_path + str(i).zfill(3), images=images)
