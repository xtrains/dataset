# import the necessary packages
import glob
import math
import numpy as np
import os
import pickle
import random
import sys

from skimage.io import imsave

from train_renderer import TrainRenderer
from utils import compress_images, get_truncated_normal

NUM_OF_SAMPLES = 500_000
RANDOM_SEED = 42
PATH_TO_CONFIG = './output/configs.pickle'
PATH_TO_IMAGES = './output/images/'
PATH_TO_IMAGES_NPZ = './output/npz_images/'
PATH_TO_BACKGROUNDS = '../mc_gill_calibrated_colour_image_database/*'
PATH_TO_CSV = './output/trains.csv'

SHAPES = ['box', 'ellipse', 'trapezoid', 'superellipse', 'hexagon', 'reinforced']
WIDTHS = [1, 1.42]
WHEELS = [2, 3, 4]
WHEEL_RADS = [2, 3, 4, 5]
C_TYPES = ['none', 'ssquare', 'msquare', 'xlsquare', 'circle', 'triangle', 'invtriangle', 'diamond', 'hexagon']
C_NUM = [1, 2, 3]
C_RADS = [2, 3, 4, 5]
C_YS = [6, 7, 8, 9, 10]
LINKER_YS = [13, 14]
HALF_SPACES = [1, 2]
ROOFS = ['open', 'flat', 'triangular', 'saw']


def number_long_wagons(train):
    res = 0
    for car in train['cars']:
        if car[1]['width'] > 1:
            res += 1
    return res


def has_long_wagon(train):
    for car in train['cars']:
        if car[1]['width'] > 1:
            return 1
    return 0


def number_passenger_cars(train):
    res = 0
    for car in train['cars']:
        if car[1]['cargo_type'] == 'circle':
            res += 1
    return res


def has_passenger_car(train):
    for car in train['cars']:
        if car[1]['cargo_type'] == 'circle':
            return 1
    return 0


def number_freight_wagons(train):
    res = 0
    for car in train['cars']:
        if car[1]['cargo_type'] != 'circle' and car[1]['cargo_type'] != 'none':
            res += 1
    return res


def has_freight_wagon(train):
    for car in train['cars']:
        if car[1]['cargo_type'] != 'circle' and car[1]['cargo_type'] != 'none':
            return 1
    return 0


def has_empty_wagon(train):
    for car in train['cars']:
        if car[1]['cargo_type'] == 'none':
            return 1
    return 0


def is_long_train(train):
    res = 0
    length = len(train['cars'])

    if length >= 3:
        res = 1
        return res

    count_long = 0
    for car in train['cars']:
        if car[1]['width'] > 1:
            count_long = count_long + 1

    if count_long >= 2:
        res = 1
    return res


def is_war_train(train):
    for car in train['cars']:
        if car[1]['shape'] == 'reinforced':
            return has_passenger_car(train)
    return 0


def is_passenger_train(train):
    count = 0
    for car in train['cars']:
        if car[1]['cargo_type'] == 'circle':
            count = count + 1
            if car[1]['width'] > 1:
                return 1
            if count >= 2:
                return 1
    return 0


def is_freight_train(train):
    count = 0
    for car in train['cars']:
        if car[1]['cargo_type'] != 'circle' and car[1]['cargo_type'] != 'none':
            count = count + 1
            if count >= 2:
                return 1
    return 0


def is_long_freight_train(train):
    if is_long_train(train) and is_freight_train(train):
        return 1
    else:
        return 0


def is_empty_train(train):
    for car in train['cars']:
        if car[1]['cargo_type'] != 'none':
            return 0
    return 1


def is_type_a_train(train):
    if is_war_train(train) or is_empty_train(train):
        return 1
    else:
        return 0


def is_type_b_train(train):
    if is_passenger_train(train) or is_long_freight_train(train):
        return 1
    else:
        return 0


def is_rural_train(train):
    if has_empty_wagon(train)\
            and (has_passenger_car(train) or has_freight_wagon(train))\
            and not has_long_wagon(train):
        return 1
    else:
        return 0


def is_mixed_train(train):
    if has_freight_wagon(train) and has_passenger_car(train) and has_empty_wagon(train):
        return 1
    else:
        return 0


def is_type_c_train(train):
    if is_rural_train(train) or is_mixed_train(train):
        return 1
    else:
        return 0


def has_roof(train, roof):
    for car in train['cars']:
        if car[1]['roof'] == roof:
            return 1
    return 0


def has_flat_roof(train):
    """Note: Hexagon wagons have no additional roof, but do have a 'flat roof'."""
    for car in train['cars']:
        if car[1]['roof'] == 'flat' or car[1]['shape'] == 'hexagon':
            return 1
    return 0


def has_open_roof(train):
    """Note: Ellipse and hexagon wagons have no additional roof, but are not 'open roof'."""
    for car in train['cars']:
        if car[1]['roof'] == 'open' and car[1]['shape'] != 'ellipse' and car[1]['shape'] != 'hexagon':
            return 1
    return 0


def has_car(train, shape):
    for car in train['cars']:
        if car[1]['shape'] == shape:
            return 1
    return 0


def has_cargo(train, cargo):
    for car in train['cars']:
        if car[1]['cargo_type'] == cargo:
            return 1
    return 0


def has_n_wheels_wagon(train, n):
    for car in train['cars']:
        if car[1]['wheels'] == n:
            return 1
    return 0


def has_wheel_radius(train, radius):
    return int(train['wheel_radius'] == radius)


def cargo_quantity(train):
    count = 0
    for car in train['cars']:
        count += car[1]['cargo_num']
    return count


def generate_csv(trains):
    """Generates a csv file with some of the relevant features of the trains, based on their configurations."""
    file = open(PATH_TO_CSV, 'w')

    file.write(
        'name,TypeA,TypeB,TypeC,WarTrain,EmptyTrain,PassengerTrain,LongFreightTrain,RuralTrain,MixedTrain,'
        'ReinforcedCar,PassengerCar,EmptyWagon,LongWagon,LongTrain,FreightTrain,FreightWagon,OpenRoofCar,HexagonCar,'
        'SuperellipseCar,TrapezoidCar,EllipseCar,BoxCar,SawRoof,TriangularRoof,FlatRoof,HexagonCargo,DiamondCargo,'
        'InvTriangleCargo,TriangleCargo,XLSquareCargo,MSquareCargo,SSquareCargo,TwoWheelsWagon,ThreeWheelsWagon,'
        'FourWheelsWagon,XSWheels,SWheels,MWheels,LWheels,CouplerHeight,WagonSpacing,Angle,TotalCargoQuantity,'
        'NumberOfWagons,NumberOfLongWagons,NumberOfFreightWagons,NumberOfPassengerCars\n')

    for i, train in enumerate(trains):
        name = str(i).zfill(7)

        number_of_long_wagons = number_long_wagons(train)
        number_of_passenger_cars = number_passenger_cars(train)
        number_of_freight_wagons = number_freight_wagons(train)

        long_wagon = int(number_of_long_wagons > 0)
        passenger_car = int(number_of_passenger_cars > 0)
        freight_wagon = int(number_of_freight_wagons > 0)
        empty_wagon = has_empty_wagon(train)

        war_train = is_war_train(train)
        empty_train = is_empty_train(train)
        passenger_train = is_passenger_train(train)
        long_train = is_long_train(train)
        freight_train = is_freight_train(train)
        long_freight_train = is_long_freight_train(train)
        rural_train = is_rural_train(train)
        mixed_train = is_mixed_train(train)
        type_a = is_type_a_train(train)
        type_b = is_type_b_train(train)
        type_c = is_type_c_train(train)

        roof_open = has_open_roof(train)

        car_reinforced = has_car(train, 'reinforced')

        hexagon_car = has_car(train, 'hexagon')
        superellipse_car = has_car(train, 'superellipse')
        trapezoid_car = has_car(train, 'trapezoid')
        ellipse_car = has_car(train, 'ellipse')
        box_car = has_car(train, 'box')

        saw_roof = has_roof(train, 'saw')
        triangular_roof = has_roof(train, 'triangular')
        flat_roof = has_flat_roof(train)

        hexagon_cargo = has_cargo(train, 'hexagon')
        diamond_cargo = has_cargo(train, 'diamond')
        inv_triangle_cargo = has_cargo(train, 'invtriangle')
        triangle_cargo = has_cargo(train, 'triangle')
        xl_square_cargo = has_cargo(train, 'xlsquare')
        m_square_cargo = has_cargo(train, 'msquare')
        s_square_cargo = has_cargo(train, 'ssquare')

        two_wheels_wagon = has_n_wheels_wagon(train, 2)
        three_wheels_wagon = has_n_wheels_wagon(train, 3)
        four_wheels_wagon = has_n_wheels_wagon(train, 4)

        xs_wheels = has_wheel_radius(train, 2)
        s_wheels = has_wheel_radius(train, 3)
        m_wheels = has_wheel_radius(train, 4)
        l_wheels = has_wheel_radius(train, 5)

        coupler_height = train['linker_y']
        wagon_spacing = train['half_space']
        angle = train['slope']
        total_cargo_quantity = cargo_quantity(train)

        number_of_wagons = len(train['cars'])
        number_of_long_wagons = number_long_wagons(train)
        number_of_freight_wagons = number_freight_wagons(train)
        number_of_passenger_cars = number_passenger_cars(train)

        file.write(
            '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},'
            '{},{},{},{},{},{},{},{},{},{},{},{},{}'
            '\n'.format(
                name, type_a, type_b, type_c, war_train, empty_train, passenger_train, long_freight_train, rural_train,
                mixed_train, car_reinforced, passenger_car, empty_wagon, long_wagon, long_train, freight_train,
                freight_wagon, roof_open, hexagon_car, superellipse_car, trapezoid_car, ellipse_car, box_car, saw_roof,
                triangular_roof, flat_roof, hexagon_cargo, diamond_cargo, inv_triangle_cargo, triangle_cargo,
                xl_square_cargo, m_square_cargo, s_square_cargo, two_wheels_wagon, three_wheels_wagon,
                four_wheels_wagon, xs_wheels, s_wheels, m_wheels, l_wheels, coupler_height, wagon_spacing, angle,
                total_cargo_quantity, number_of_wagons, number_of_long_wagons, number_of_freight_wagons,
                number_of_passenger_cars))

    file.close()


def gen_cargo_settings(train, cargo_type, width):
    """Generates the configuration of a wagon's cargo."""
    cargo_num, cargo_rad, cargo_edge, cargo_y = (0, 0, 0, 0)

    if width > 1:  # long train
        if cargo_type == 'msquare':
            cargo_num = np.random.choice(C_NUM, p=[0.65, 0.35, 0.0])
            cargo_rad = 2
            cargo_edge = 10
        elif cargo_type == 'xlsquare':
            cargo_num = 1
            cargo_rad = 2
            cargo_edge = 9
        elif cargo_type == 'hexagon':
            cargo_num = np.random.choice(C_NUM, p=[0.65, 0.35, 0.0])
            if cargo_num == 2:
                cargo_rad = 2
                cargo_edge = np.random.choice([10, 11])
            else:  # only one hexagonal cargo
                cargo_rad = np.random.choice(C_RADS, p=[0.3, 0.4, 0.3, 0.0])
                cargo_edge = 9

        else:  # any other cargo shape
            cargo_rad = np.random.choice(C_RADS, p=[0.27, 0.3, 0.38, 0.05])
            if cargo_rad == 2:
                cargo_num = np.random.choice(C_NUM, p=[0.3, 0.3, 0.4])
                if cargo_num == 3:
                    cargo_edge = np.random.choice([9, 10])
                else:
                    cargo_edge = np.random.choice([8, 9, 10, 11, 12])
            elif cargo_rad == 3:
                cargo_num = np.random.choice(C_NUM, p=[0.55, 0.35, 0.1])
                if cargo_num == 3:
                    cargo_edge = 8
                else:
                    cargo_edge = np.random.choice([8, 9, 10])
            elif cargo_rad == 4:
                cargo_num = np.random.choice(C_NUM, p=[0.6, 0.4, 0.0])
                cargo_edge = 8
            elif cargo_rad == 5:
                cargo_num = 1
                cargo_edge = 8

    else:  # short train
        if cargo_type == 'msquare':
            cargo_num = 1
            cargo_rad = 2
            cargo_edge = 9
        elif cargo_type == 'hexagon':
            cargo_num = 1
            cargo_rad = np.random.choice(C_RADS, p=[0.8, 0.2, 0.0, 0.0])
            cargo_edge = 9
        else:  # any other cargo shape
            cargo_rad = np.random.choice(C_RADS, p=[0.4, 0.3, 0.3, 0.0])
            if cargo_rad == 2:
                cargo_num = np.random.choice(C_NUM, p=[0.8, 0.2, 0.0])
                if cargo_num == 2:
                    cargo_edge = 7
                else:
                    cargo_edge = 9
            else:  # cargo rad 3, 4 or 5
                cargo_num = 1
                cargo_edge = 9

    if cargo_rad == 2:
        cargo_y = np.random.choice(C_YS, p=[0.05, 0.2, 0.3, 0.25, 0.2])
    elif cargo_rad == 3:
        cargo_y = np.random.choice(C_YS, p=[0.0, 0.2, 0.3, 0.3, 0.2])
    elif cargo_rad == 4:
        cargo_y = np.random.choice(C_YS, p=[0.0, 0.0, 0.5, 0.5, 0.0])

    if (cargo_rad == 5) or (train['wheel_radius'] == 5):
        cargo_y = 8

    if (train['wheel_radius'] == 5) and (cargo_rad == 5):
        train['wheel_radius'] = 4
        train['wheel_base'] = train['wheel_base'] + 1

    return cargo_num, cargo_rad, cargo_edge, cargo_y


def generate_car(train):
    """Generates the configuration of a train's wagon."""
    car = {}
    wheel_radius = train['wheel_radius']
    width = np.random.choice(WIDTHS, p=[2/3, 1/3])
    cargo_type = 'none'
    wheels = 2
    roof = 'open'

    if width > 1:  # long train
        if wheel_radius == 2:
            wheels = np.random.choice(WHEELS, p=[0.0, 0.8, 0.2])
        elif wheel_radius == 3:
            wheels = np.random.choice(WHEELS, p=[0.5, 0.4, 0.1])
        elif wheel_radius == 4:
            wheels = np.random.choice(WHEELS, p=[0.5, 0.5, 0.0])
        elif wheel_radius == 5:
            wheels = np.random.choice(WHEELS, p=[0.5, 0.5, 0.0])

        shape = np.random.choice(SHAPES, p=[0.1, 0.2, 0.1, 0.2, 0.2, 0.2])

        if shape == 'box':
            roof = np.random.choice(ROOFS, p=[0.2, 0.3, 0.2, 0.3])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.12, 0.12, 0.11, 0.1, 0.1, 0.1, 0.1, 0.11])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.12, 0.12, 0.11, 0.14, 0.1, 0.1, 0.1, 0.11])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.11, 0.11, 0.1, 0.16, 0.11, 0.11, 0.11, 0.09])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.11, 0.11, 0.1, 0.16, 0.1, 0.1, 0.1, 0.12])

        elif shape == 'ellipse':
            roof = 'open'
            cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.11, 0.1, 0.1, 0.17, 0.11, 0.11, 0.1, 0.1])
            if wheel_radius == 2:
                train['wheel_base'] = train['wheel_base'] - 1
                train['wheel_radius'] = 3

        elif shape == 'trapezoid':
            roof = np.random.choice(ROOFS, p=[0.4, 0.2, 0.2, 0.2])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.15, 0.11, 0.1, 0.1, 0.1, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.11, 0.11, 0.11, 0.13, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.13, 0.1, 0.1, 0.1, 0.14, 0.11, 0.11, 0.11, 0.1])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.11, 0.1, 0.1, 0.1, 0.14, 0.11, 0.11, 0.11, 0.12])

        elif shape == 'superellipse':
            roof = np.random.choice(ROOFS, p=[0.55, 0.15, 0.15, 0.15])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.2, 0.09, 0.09, 0.09, 0.13, 0.1, 0.1, 0.1, 0.1])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.09, 0.09, 0.09, 0.15, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.09, 0.09, 0.09, 0.16, 0.11, 0.11, 0.11, 0.1])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.12, 0.09, 0.09, 0.09, 0.16, 0.11, 0.11, 0.11, 0.12])

        elif shape == 'hexagon':
            roof = 'open'
            cargo_type = np.random.choice(C_TYPES, p=[0.11, 0.11, 0.1, 0.1, 0.1, 0.12, 0.12, 0.12, 0.12])

        elif shape == 'reinforced':
            roof = np.random.choice(ROOFS, p=[0.25, 0.25, 0.25, 0.25])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.12, 0.12, 0.11, 0.1, 0.1, 0.1, 0.1, 0.11])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.12, 0.12, 0.11, 0.14, 0.1, 0.1, 0.1, 0.11])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.11, 0.11, 0.1, 0.16, 0.11, 0.11, 0.11, 0.09])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.1, 0.11, 0.11, 0.1, 0.16, 0.1, 0.1, 0.1, 0.12])

    else:  # short train
        if wheel_radius == 2:
            wheels = np.random.choice(WHEELS, p=[0.1, 0.9, 0.0])
        elif wheel_radius == 3:
            wheels = np.random.choice(WHEELS, p=[0.9, 0.1, 0.0])
        elif wheel_radius == 4:
            wheels = 2
        elif wheel_radius == 5:
            wheels = 2

        shape = np.random.choice(SHAPES, p=[0.2, 0.1, 0.2, 0.2, 0.1, 0.2])

        if shape == 'box':
            roof = np.random.choice(ROOFS, p=[0.2, 0.3, 0.2, 0.3])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.2, 0.14, 0.11, 0.0, 0.11, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.2, 0.13, 0.11, 0.0, 0.15, 0.1, 0.1, 0.1, 0.11])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.13, 0.13, 0.1, 0.0, 0.19, 0.12, 0.12, 0.11, 0.1])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.13, 0.1, 0.0, 0.19, 0.11, 0.11, 0.1, 0.12])

        elif shape == 'ellipse':
            roof = 'open'
            cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.12, 0.11, 0.0, 0.19, 0.11, 0.11, 0.11, 0.11])
            if wheel_radius == 2:
                train['wheel_base'] = train['wheel_base'] - 1
                train['wheel_radius'] = 3

        elif shape == 'trapezoid':
            roof = np.random.choice(ROOFS, p=[0.4, 0.2, 0.2, 0.2])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.2, 0.12, 0.1, 0.0, 0.1, 0.12, 0.12, 0.12, 0.12])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.15, 0.12, 0.12, 0.0, 0.15, 0.12, 0.11, 0.11, 0.12])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.15, 0.11, 0.11, 0.0, 0.16, 0.12, 0.12, 0.12, 0.11])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.11, 0.11, 0.0, 0.15, 0.12, 0.12, 0.12, 0.13])

        elif shape == 'superellipse':
            roof = np.random.choice(ROOFS, p=[0.55, 0.15, 0.15, 0.15])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.23, 0.1, 0.1, 0.0, 0.13, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.18, 0.1, 0.1, 0.0, 0.16, 0.12, 0.11, 0.11, 0.12])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.17, 0.1, 0.1, 0.0, 0.18, 0.12, 0.11, 0.11, 0.11])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.16, 0.12, 0.1, 0.0, 0.17, 0.11, 0.11, 0.11, 0.12])

        elif shape == 'hexagon':
            roof = 'open'
            cargo_type = np.random.choice(C_TYPES, p=[0.15, 0.13, 0.1, 0.0, 0.1, 0.13, 0.13, 0.13, 0.13])

        elif shape == 'reinforced':
            roof = np.random.choice(ROOFS, p=[0.25, 0.25, 0.25, 0.25])
            if roof == 'open':
                cargo_type = np.random.choice(C_TYPES, p=[0.19, 0.15, 0.11, 0.0, 0.11, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'flat':
                cargo_type = np.random.choice(C_TYPES, p=[0.15, 0.15, 0.12, 0.0, 0.14, 0.11, 0.11, 0.11, 0.11])
            elif roof == 'triangular':
                cargo_type = np.random.choice(C_TYPES, p=[0.13, 0.14, 0.11, 0.0, 0.19, 0.11, 0.11, 0.11, 0.1])
            elif roof == 'saw':
                cargo_type = np.random.choice(C_TYPES, p=[0.14, 0.14, 0.11, 0.0, 0.19, 0.1, 0.1, 0.1, 0.12])

    car['shape'] = shape
    car['height'] = 1
    car['width'] = width
    car['wheels'] = wheels
    car['cargo_type'] = cargo_type
    car['roof'] = roof

    if cargo_type != 'none':
        (cargo_num, cargo_rad, cargo_edge, cargo_y) = gen_cargo_settings(train, cargo_type, width)

        car['cargo_num'] = cargo_num
        car['cargo_edge'] = cargo_edge
        car['cargo_rad'] = cargo_rad
        car['cargo_y'] = cargo_y
    else:
        car['cargo_num'] = 0

    return car


def generate_train_config():
    """Generates the configuration of a train."""
    train = {}

    wheel_radius = np.random.choice(WHEEL_RADS, p=[0.1, 0.45, 0.4, 0.05])

    train['base_width'] = 24
    train['base_height'] = 20
    train['roof_height'] = 3
    train['wheel_base'] = train['base_height'] - train['roof_height'] - wheel_radius - 1
    train['wheel_radius'] = wheel_radius
    train['linker_y'] = np.random.choice(LINKER_YS)
    train['half_space'] = np.random.choice(HALF_SPACES)
    train['background'] = np.random.choice(backgrounds)

    # num_cars varies between 1 and 4
    dist = get_truncated_normal(mean=0, sd=0.5, low=-1, upp=1)
    num_cars = dist.rvs(size=1)
    num_cars = num_cars * 2 + 3
    num_cars = num_cars.astype(int)
    num_cars = num_cars[0]

    cars = []
    for i in range(0, num_cars):
        car = generate_car(train)
        cars.append(('c{}'.format(i), car))
    train['cars'] = cars

    return train


def render(config, output_file):
    """Renders a train based on its configuration and saves the rendered image to output_file."""
    tr = TrainRenderer(config)
    img = tr.render_train()
    imsave(PATH_TO_IMAGES + output_file, img)


def generate_config_file(filename, configs):
    """Generates a configuration file to store the trains' configurations."""
    with open(filename, 'wb') as config_file:
        pickle.dump(configs, config_file)
        config_file.close()


# if needed create folders
os.makedirs(PATH_TO_IMAGES, exist_ok=True)
os.makedirs(PATH_TO_IMAGES_NPZ, exist_ok=True)

# initialize random seeds
random.seed(RANDOM_SEED)
np.random.seed(RANDOM_SEED)

# store the trains' configurations
configs = []

print("[INFO] Loading background images...")
backgrounds = [f for f in glob.glob(PATH_TO_BACKGROUNDS)]

for sample_num in range(0, NUM_OF_SAMPLES):
    if sample_num % math.ceil(NUM_OF_SAMPLES * 0.05) == 0:
        print("[INFO] Generating example: {} of {}".format(sample_num, NUM_OF_SAMPLES))

    config = generate_train_config()
    configs.append(config)
    image_file = '{}.png'.format(str(sample_num).zfill(7))

    try:
        render(config, image_file)
    except Exception:
        print("[ERROR] Error in rendering index: {}".format(sample_num), file=sys.stderr)

print("[INFO] Generating config file...")
generate_config_file(PATH_TO_CONFIG, configs)

print("[INFO] Generating CSV file...")
generate_csv(configs)

print("[INFO] Compressing images...")
compress_images(PATH_TO_IMAGES, PATH_TO_IMAGES_NPZ)
