# Explainable Abstract Trains Dataset
 
## Overview
 
The **Explainable Abstract Trains Dataset** is an image dataset containing
 simplified representations of trains.
It aims to provide a platform for the application and research of algorithms
 for justification and explanation extraction. The dataset contains 500 000
 images of 152 x 152 pixel-art representations of trains, each annotated with
 multiple attributes describing the trains' features and bounding boxes for each
 train element.
 The dataset is accompanied by an ontology that conceptualizes and classifies
 the depicted trains based on their visual characteristics, allowing for a
 precise understanding of how each train was labeled. 

A detailed description of the dataset can be found in this
 [technical report](https://arxiv.org/abs/2012.12115). 
 

![Image Sample](image_samples/0000532.png)
![Image Sample](image_samples/0000033.png)
![Image Sample](image_samples/0000161.png)
![Image Sample](image_samples/0000072.png)
![Image Sample](image_samples/0000424.png)
![Image Sample](image_samples/0000431.png)
![Image Sample](image_samples/0002335.png)
![Image Sample](image_samples/0000761.png)
 
## Technical Report

If you use the Explainable Abstract Trains Dataset, please consider citing the
following publication:

* Manuel de Sousa Ribeiro, Ludwig Krippahl, and Joao Leite,
 [Explainable Abstract Trains Dataset](https://arxiv.org/abs/2012.12115).
 arXiv:2012.12115, 2020.

 
## Download
The Explainable Abstract Trains Dataset is available to download at:
 https://xtrains.di.fct.unl.pt/xtrains_dataset.zip
 
 
## How to Generate the Dataset

This repository contains the set of scripts used to generate the Explainable
 Abstract Trains Dataset.

### Requirements

The scripts can be run using the [conda](https://docs.conda.io/) package and 
environment manager.

```bash
conda create --name explainable_abstract_trains_dataset --file requirements.txt
conda activate explainable_abstract_trains_dataset
python dataset_generator.py
```

A folder  called `mc_gill_calibrated_colour_image_database` is assumed to exist
 outside this folder, containing the complete set of images from the
 'Landscapes' category of McGill Calibrated Colour Image Database, visit their
 [website](http://tabby.vision.mcgill.ca/html/browsedownload.html) to download
 the images.

## Pre-trained Models

The folder
 ['trained_models'](https://bitbucket.org/xtrains/dataset/src/master/trained_models/)
 contains tensorflow pre-trained models (.hdf5) for different types of trains existing
 in the dataset.